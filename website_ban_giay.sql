-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 03, 2022 lúc 06:43 AM
-- Phiên bản máy phục vụ: 10.4.25-MariaDB
-- Phiên bản PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `website_ban_giay`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_11_14_133558_create_tbl_admin_table', 2),
(6, '2022_11_14_135136_create_tbl_category_table', 3),
(7, '2022_11_20_082957_create_tbl_thuonghieu_table', 4),
(8, '2022_11_20_083937_create_tbl_thuonghieu1_table', 5),
(9, '2022_11_21_065713_create_tbl_sanpham_table', 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `password`, `address`, `phone`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nguyen dinh hau', 'abc@gmail.com', '12345678', 'bac giang', '0123456789', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hienthi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`, `mota`, `hienthi`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'sneaker', '10 tot', '0', NULL, NULL, NULL),
(10, 'giay banh mi', 'good', '0', NULL, NULL, NULL),
(11, 'giay the thao', 'good', '0', NULL, NULL, NULL),
(12, 'giay da', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một.', '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_sanpham`
--

CREATE TABLE `tbl_sanpham` (
  `pro_id` bigint(20) UNSIGNED NOT NULL,
  `pro_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate_id` int(11) NOT NULL,
  `thuonghieu_id` int(11) NOT NULL,
  `pro_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_sanpham`
--

INSERT INTO `tbl_sanpham` (`pro_id`, `pro_name`, `cate_id`, `thuonghieu_id`, `pro_desc`, `pro_content`, `pro_price`, `pro_image`, `pro_status`, `created_at`, `updated_at`) VALUES
(11, 'nike 1', 11, 2, 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một.', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một.', '12000000', '211.jfif', '0', NULL, NULL),
(12, 'nike 2', 10, 4, 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một.', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một.', '13200000', '314112041_126184473584757_7916246225787842352_n77.jpg', '0', NULL, NULL),
(13, 'adidas', 11, 2, 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một', '3000000', '314833057_124856400381543_873552500447393969_n36.jpg', '0', NULL, NULL),
(14, 'adidas2', 7, 2, '102', 'Các định nghĩa của văn học đã thay đổi theo thời gian: đó là một \"định nghĩa phụ thuộc vào văn hóa\".[1] Ở Tây Âu trước thế kỷ 18, văn học biểu thị tất cả các cuốn sách và văn bản. Một ý nghĩa hạn chế hơn của thuật ngữ này xuất hiện trong thời kỳ Lãng mạn, trong đó nó bắt đầu phân định là các tác phẩm viết \"giàu trí tưởng tượng\"', '13200000', '315215642_126184543584750_8423056829302587090_n19.jpg', '0', NULL, NULL),
(15, 'nike 2', 7, 2, 'Các định nghĩa của văn học đã thay đổi theo thời gian: đó là một \"định nghĩa phụ thuộc vào văn hóa\".[1] Ở Tây Âu trước thế kỷ 18, văn học biểu thị tất cả các cuốn sách và văn bản. Một ý nghĩa hạn chế hơn của thuật ngữ này xuất hiện trong thời kỳ Lãng mạn, trong đó nó bắt đầu phân định là các tác phẩm viết \"giàu trí tưởng tượng\"', 'Các định nghĩa của văn học đã thay đổi theo thời gian: đó là một \"định nghĩa phụ thuộc vào văn hóa\".[1] Ở Tây Âu trước thế kỷ 18, văn học biểu thị tất cả các cuốn sách và văn bản. Một ý nghĩa hạn chế hơn của thuật ngữ này xuất hiện trong thời kỳ Lãng mạn, trong đó nó bắt đầu phân định là các tác phẩm viết \"giàu trí tưởng tượng\"', '3000000', '584.jfif', '0', NULL, NULL),
(16, 'nike fake', 7, 2, 'Các định nghĩa của văn học đã thay đổi theo thời gian: đó là một \"định nghĩa phụ thuộc vào văn hóa\".[1] Ở Tây Âu trước thế kỷ 18, văn học biểu thị tất cả các cuốn sách và văn bản. Một ý nghĩa hạn chế hơn của thuật ngữ này xuất hiện trong thời kỳ Lãng mạn, trong đó nó bắt đầu phân định là các tác phẩm viết \"giàu trí tưởng tượng\"', 'Các định nghĩa của văn học đã thay đổi theo thời gian: đó là một \"định nghĩa phụ thuộc vào văn hóa\".[1] Ở Tây Âu trước thế kỷ 18, văn học biểu thị tất cả các cuốn sách và văn bản. Một ý nghĩa hạn chế hơn của thuật ngữ này xuất hiện trong thời kỳ Lãng mạn, trong đó nó bắt đầu phân định là các tác phẩm viết \"giàu trí tưởng tượng\"', '13200000', '314683288_126184486918089_8055143714096170934_n79.jpg', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_thuonghieu`
--

CREATE TABLE `tbl_thuonghieu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trade_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trade_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trade_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_thuonghieu`
--

INSERT INTO `tbl_thuonghieu` (`id`, `trade_name`, `trade_desc`, `trade_status`, `created_at`, `updated_at`) VALUES
(2, 'adidas', 'good', '0', NULL, NULL),
(3, 'sneaker', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một. Em vẫn nhớ mãi giây phút mà mẹ đã mang đôi giày ấy đến với thế giới của em.', '0', NULL, NULL),
(4, 'yody', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một.', '0', NULL, NULL),
(5, 'Nyken', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một. Em vẫn nhớ mãi giây phút mà mẹ đã mang đôi giày ấy đến với thế giới của em.', '0', NULL, NULL),
(6, 'China', 'Đôi giày ấy mẹ đã mua cho em vào ngày đầu tiên đi học, cùng em bước tới trường trở thành cô học sinh lớp một. Em vẫn nhớ mãi giây phút mà mẹ đã mang đôi giày ấy đến với thế giới của em.', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_sanpham`
--
ALTER TABLE `tbl_sanpham`
  ADD PRIMARY KEY (`pro_id`);

--
-- Chỉ mục cho bảng `tbl_thuonghieu`
--
ALTER TABLE `tbl_thuonghieu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `tbl_sanpham`
--
ALTER TABLE `tbl_sanpham`
  MODIFY `pro_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `tbl_thuonghieu`
--
ALTER TABLE `tbl_thuonghieu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
